import { getData } from "./methods/get.mjs";
import { postData } from "./methods/post.mjs";
import { putData } from "./methods/put.mjs";
import { patchData } from "./methods/patch.mjs";
import { deleteData } from "./methods/delete.mjs";
import { getOptions } from "./methods/options.mjs";
import { renderHTML } from "./helpers.mjs";

(() => {
  renderHTML();

  const methodRef = document.querySelector("#method");
  const urlRef = document.querySelector("#url");
  const btnRef = document.querySelector("#send");
  const userInputRef = document.querySelector("#userInput");
  const userInputAreaRef = document.querySelector("#userInputArea");

  methodRef.onchange = () => {
    methodRef.value == "post" ||
    methodRef.value == "put" ||
    methodRef.value == "patch"
      ? userInputAreaRef.classList.remove("isHidden")
      : userInputAreaRef.classList.add("isHidden");
  };

  urlRef.oninput = () => {
    if (urlRef.value !== "") {
      btnRef.classList.remove("disabledBtn");
      btnRef.classList.add("activeBtn");
      btnRef.removeAttribute("disabled");
    } else {
      btnRef.setAttribute("disabled", "true");
      btnRef.classList.add("disabledBtn");
    }
  };

  btnRef.addEventListener("click", e => {
    e.preventDefault();
    const method = methodRef.value;
    const url = urlRef.value;
    const userRef =
      userInputRef.value !== "" ? JSON.parse(userInputRef.value) : "";

    switch (method) {
      case "get":
        getData(url);
        break;
      case "post":
        postData(url, userRef);
        break;
      case "put":
        putData(url, userRef);
        break;
      case "patch":
        patchData(url, userRef);
        break;
      case "delete":
        deleteData(url);
        break;
      case "options":
        getOptions(url);
        break;
    }
  });
})();
