export const showResults = result => {
  //console.log(result);
  const resultsRef = document.querySelector("#response");
  const resultsStatusRef = document.querySelector("#responseStatus");

  const status = JSON.stringify(result.status, null, 2);
  const statusText = JSON.stringify(result.statusText, null, 2);

  resultsRef.innerHTML = JSON.stringify(result.data, null, 2);
  resultsStatusRef.innerHTML = `Status: ${status} ${statusText}`;

  if (status[0] == 2) {
    resultsStatusRef.style.backgroundColor = "green";
  } else if (status[0] == 4) {
    resultsStatusRef.style.backgroundColor = "red";
  }
};
