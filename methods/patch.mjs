import { showResults } from "../show.mjs";

export const patchData = (url, userInput) => {
  async function patchData() {
    return await axios
      .patch(url, userInput)
      .then(resp => resp)
      .catch(error => error.response);
  }

  const response = patchData();
  response.then(data => showResults(data));
};
