import { showResults } from "../show.mjs";

export const getData = url => {
  async function getData() {
    return await axios
      .get(url)
      .then(resp => resp)
      .catch(error => error.response);
  }

  const response = getData();
  response.then(data => showResults(data));
};
