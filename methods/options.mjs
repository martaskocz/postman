import { showResults } from "../show.mjs";

export const getOptions = url => {
  async function getOptions() {
    return await axios
      .options(url)
      .then(resp => resp.headers)
      .catch(error => error.response);
  }

  const response = getOptions();
  response.then(data => showResults(data));
};
