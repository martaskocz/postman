import { showResults } from "../show.mjs";

export const putData = (url, userInput) => {
  async function putData() {
    return await axios
      .put(url, userInput)
      .then(resp => resp)
      .catch(error => error.response);
  }

  const response = putData();
  response.then(data => showResults(data));
};
