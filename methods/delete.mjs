import { showResults } from "../show.mjs";

export const deleteData = url => {
  async function deleteData() {
    return await axios
      .delete(url)
      .then(resp => resp)
      .catch(error => error.response);
  }

  const response = deleteData();
  response.then(data => showResults(data));
};
