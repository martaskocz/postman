import { showResults } from "../show.mjs";

export const postData = (url, userInput) => {
  async function postData() {
    return await axios
      .post(url, userInput)
      .then(resp => resp)
      .catch(error => error.response);
  }

  const response = postData();
  response.then(data => showResults(data));
};
