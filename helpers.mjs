export const renderHTML = () => {
  renderMainBar();
  renderUserInput();
  renderResponseArea();
};

const renderMainBar = () => {
  const divBox = document.createElement("div");
  divBox.classList.add("box");
  document.body.appendChild(divBox);
  const select = document.createElement("select");
  select.setAttribute("id", "method");
  divBox.appendChild(select);

  const generateMethod = method => {
    const option = document.createElement("option");
    option.setAttribute("value", method);
    option.innerHTML = method.toUpperCase();
    select.appendChild(option);
  };

  const options = ["get", "post", "put", "patch", "delete", "options"];
  options.forEach(generateMethod);

  const input = document.createElement("input");
  input.setAttribute("type", "text");
  input.setAttribute("id", "url");
  input.setAttribute("placeholder", "e.g. http://localhost:3000/zombies");
  divBox.appendChild(input);

  const button = document.createElement("button");
  button.setAttribute("id", "send");
  button.setAttribute("disabled", "true");
  button.classList.add("disabledBtn");
  button.innerHTML = "Wyslij";
  divBox.appendChild(button);
};

const renderResponseArea = () => {
  const responseArea = document.createElement("div");
  responseArea.setAttribute("id", "responseArea");
  document.body.appendChild(responseArea);

  const responseStatDesc = document.createElement("p");
  responseStatDesc.setAttribute("id", "responseDesc");
  responseStatDesc.classList.add("description");
  responseStatDesc.innerHTML = "Response Status";
  responseArea.appendChild(responseStatDesc);

  const responseStatus = document.createElement("div");
  responseStatus.setAttribute("id", "responseStatus");
  responseArea.appendChild(responseStatus);

  const responseDesc = document.createElement("p");
  responseDesc.setAttribute("id", "responseDesc");
  responseDesc.classList.add("description");
  responseDesc.innerHTML = "Response Body";
  responseArea.appendChild(responseDesc);

  const response = document.createElement("pre");
  response.setAttribute("id", "response");
  responseArea.appendChild(response);
};

const renderUserInput = () => {
  const responseStatus = document.getElementById("responseArea");
  const userInputArea = document.createElement("div");
  const userInputDesc = document.createElement("p");
  const userInput = document.createElement("textarea");

  userInputArea.setAttribute("id", "userInputArea");

  userInputDesc.setAttribute("id", "userInputDesc");
  userInputDesc.classList.add("description");
  userInputDesc.innerHTML = "Please enter data:";
  userInput.setAttribute("id", "userInput");

  userInputArea.classList.add("isHidden");
  userInputArea.appendChild(userInputDesc);
  userInputArea.appendChild(userInput);
  document.body.insertBefore(userInputArea, responseStatus);
};
